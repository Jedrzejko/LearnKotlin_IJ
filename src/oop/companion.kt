package oop

interface HouseFactory {
    fun createHouse(): House
}

class House(val roomsNumber: Int, val price: Double) {
    companion object : HouseFactory {
        val HOUSES_FOR_SALE = 10
        fun getNormalHouse() = House(6, 12_999.0)
        fun getLuxuryHouse() = House(23, 12_112_999.0)
        override fun createHouse() = getNormalHouse()
    }
}

fun main(args: Array<String>) {
    val normalHouse = House.getNormalHouse()
    println(normalHouse.roomsNumber)
    println(normalHouse.price)
    println(House.getLuxuryHouse().roomsNumber)
    println(House.HOUSES_FOR_SALE)
    println(House.createHouse().price)
}