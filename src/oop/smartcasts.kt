package oop

fun Bicycle.replaceWheel() {
    println("Replacing wheel...")
}

fun Boat.startEngine(): Boolean {
    print("strarting engine...")
    return true
}

fun main(args: Array<String>) {
    val vehicle: Drivable = Bicycle()

    //instanceof :: is
    if (vehicle is Bicycle) {
        vehicle.replaceWheel()
    } else if (vehicle is Boat) {
        vehicle.startEngine()
    }

    if (vehicle is Boat && vehicle.startEngine()) {
        //...
    }

    if (vehicle !is Boat || vehicle.startEngine()) {
        // ...
    }

    if (vehicle !is Bicycle) {
        return
    }

    vehicle.replaceWheel()
}