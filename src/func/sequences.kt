package func

import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    val lonList = (1..999999L).toList()
    var sum = 0L
    val msNonSeq = measureTimeMillis {
        sum = lonList
                .filter { it > 50 }
                .map { it * 2 }
                .map { it / 3 }
                .map { it + 5 }
                .take(10000)
                .sum()
    }
    println("msNonSeq: $sum  in: $msNonSeq")
    val msSeq = measureTimeMillis {
        sum = lonList
                .asSequence()
                .filter { it > 50 }
                .map { it * 2 }
                .map { it / 3 }
                .map { it + 5 }
                .take(10000)
                .sum()
    }
    println("msSeq   : $sum  in: $msSeq")
    val msSeqInv = measureTimeMillis {
        sum = lonList
                .asSequence()
                .filter { it > 50 }
                .take(10000)
                .map { it * 2 }
                .map { it / 3 }
                .map { it + 5 }
                .sum()
    }
    println("msSeqInv: $sum  in: $msSeqInv")
}