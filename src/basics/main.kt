package basics

import java.util.Random

fun main(args: Array<String>) {

    //challenge1 (sec:3:20)
    val name = readLine()
    val greetings = if (getNameLength(name) > 0) {
        "Hello $name"
    } else {
        "YO! YO! John Doe"
    }
    print(greetings)

    //challenge2 (sec:3:21)
    val rands: ArrayList<Int> = arrayListOf()
    for (i in 1..100) {
        rands.add(Random().nextInt(100))
    }
    println()
    println(rands.filter { rand -> rand <= 10 })

    //challenge1 (sec:4:33)
    // Some faulty data with ages of our users
    val data = mapOf(
            "users1.csv" to listOf(32, 45, 17, -1, 34),
            "users2.csv" to listOf(19, -1, 67, 22),
            "users3.csv" to listOf(),
            "users4.csv" to listOf(56, 32, 18, 44)
    )
    //ad.1
    val avgAge = data.flatMap { it.value }.filter { it > 0 }.average()
    println("Avg ages: %.2f".format(avgAge))
    //ad.2
    val faultDatas = data.filter { it -> it.value.any { it <= 0 } }.map { it.key }
    println("Has wrong data: $faultDatas")
    //ad.3
    val faultEntries = data.flatMap { it.value }.count { it <= 0 }
    println("Fault entries: $faultEntries")
}

fun getNameLength(name: String?): Int {
    return name?.length ?: -1
}
