package oop

class Country(val name: String, val areaSqKm: Int) {
    constructor(name: String) : this(name, 5) {
        println("Custom constructor")
    }

    fun print() = "$name, $areaSqKm km^2"
}

fun main(args: Array<String>) {
    val australia = Country("Australia", 7_700_00)
    println(australia.name)
    println(australia.areaSqKm)
    println(australia.print())
    val spain = Country("Spain")
    println(spain.print())
}