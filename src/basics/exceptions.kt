package basics

import java.io.IOException

fun main(args: Array<String>) {
    val input = try {
        getExtInput()
    } catch (e: IOException) {
        e.printStackTrace()
        "WORNG"
    } finally {
        println("done")
    }
    println(input)
}

fun getExtInput(): String {
    throw IOException("Didn't read!")
}