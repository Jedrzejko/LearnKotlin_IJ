package oop

class Source<out T>(private val t: T) {
    fun produceT(): T {
        return t
    }
}

class Sink<in T> {
    fun add(t: T) {
        //...
    }
}

fun main(args: Array<String>) {
    val strSource: Source<String> = Source("asd")
    val anySource: Source<Any> = strSource
    println(anySource.produceT())
    val anySink: Sink<Any> = Sink()
    val strSink: Sink<String> = anySink
    strSink.add("asd")
}