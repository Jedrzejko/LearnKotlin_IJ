package func

fun main(argd: Array<String>) {
    val list = (1..10).toList()
    val first5 = list.take(5)
    val wo9 = list.drop(9)
    println(first5)
    println(wo9)

    val list2 = generateSequence(0) { it + 5 }
    val l2First10 = list2.take(10).toList()
    println(l2First10)

    val list3 = generateSequence(0) {
        println("Calc: ${it+10}")
        it + 5
    }
    val l3f5 = list3.take(5).toList()
    val l3f10 = list3.take(10).toList()
    println(l3f5)
    println(l3f10)


}