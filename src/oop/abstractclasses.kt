package oop

abstract class BaseShape(val name: String) {
    abstract fun area(): Double
}

class MainCircle(name: String, val radius: Double) : BaseShape(name) {
    override fun area(): Double = Math.PI * Math.pow(radius, 2.0)
}

fun main(args: Array<String>) {
    val shape = MainCircle("MainCircle", 2.0)
    println(shape.area())
}