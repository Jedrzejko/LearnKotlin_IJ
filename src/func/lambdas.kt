package func

fun main(args: Array<String>) {
    val timesTwo = { x: Int -> x * 2 }
    val add: (Int, Int) -> Int = { x: Int, y: Int -> x + y }

    val list = (1..100).toList()

    println(list.filter({ el -> el % 3 == 0 }))
    println(list.filter({ it % 3 == 0 }))
    println(list.filter { it.even() })
    println(list.filter(::isEven))
    println(timesTwo)
    println(add)
}

fun isEven(i: Int) = i % 2 == 0

fun Int.even() = this % 2 == 0