package func

import java.io.FileReader

fun main(args: Array<String>) {
    FileReader("file.txr").use {
        val str = it.readText()
        println(str)
    }
}