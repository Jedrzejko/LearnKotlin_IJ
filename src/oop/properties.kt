package oop

class City {
    var name: String = ""
        get() = field
        set(value) {
            if (value.isNotBlank()) {
                field = value
            }
        }
    var population: Int = 0
}

fun main(args: Array<String>) {
    val city = City()
    city.name = "Warsaw"
    city.name = "   "
    city.population = 2_200_000

    println(city.name)
    println(city.population)
}