package basics

fun main(args: Array<String>) {
    val list = (1..10).toList()
    val double = list.map { el -> el * 2 }
    println(double)
    val avg = list.average()
    val shift = list.map { it - avg }
    println(shift)

    val nestedList = listOf(
            (1..5).toList(),
            (6..10).toList(),
            (11..15).toList()
    )

    val notFlat = nestedList.map { it.sortedDescending() }
    println(notFlat)
    val flat = nestedList.flatMap { it.sortedDescending() }
    println(flat)
}