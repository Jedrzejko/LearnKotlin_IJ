package oop

// private/protected - same as in java
// no package-private = public(default)
// internal - visible inside the same module

private open class Car(val brand: String, private val model: String) {
    protected fun showModel() = model
}

fun main(args: Array<String>) {
    val car = Car("Sudupi", "Gantaro")
    car.brand
}