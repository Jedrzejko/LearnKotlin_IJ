package oop

fun Int.isEven(): Boolean = (this % 2 == 0)

fun City.isLarge() = population > 1_000_000

fun main(args: Array<String>) {
    println(5.isEven())
    println(4.isEven())

    val natural = listOf(51,98,51,98,198,198,51,65,16,8541,851,9681,651)
    println(natural.filter { it.isEven() })

    val warsw = City()
    warsw.name = "Warsaw"
    warsw.population = 233_234_234
    println(warsw.isLarge())
}