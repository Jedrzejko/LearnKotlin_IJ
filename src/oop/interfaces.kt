package oop

interface Drivable {
    fun drive() {
        println("Driving interface")
    }
}

class Bicycle : Drivable {
    override fun drive() {
        println("I'm driving bicycle")
    }
}

class Boat : Drivable {
    override fun drive() {
        println("I'm driving a boat")
    }

    fun a() = 1
}

fun main(args: Array<String>) {
    val driveable: Drivable = Bicycle()
    driveable.drive()
    val driveable1: Drivable = Boat()
    driveable1.drive()
    val driveable2 = Boat()
    driveable2.a()
}