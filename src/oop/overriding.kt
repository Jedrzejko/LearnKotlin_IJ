package oop

abstract class Vehicle(open val brand: String = "") {
    open fun drive() {
        println("Driving vehicle")
    }

    abstract fun honk()
}

class Sedan(override var brand: String = "SedanBrand") : Vehicle(), Drivable {
    override fun drive() {
        super<Drivable>.drive()
    }

    override fun honk() {
        println("HONK! HONK!")
    }
}

fun main(args: Array<String>) {
    val sedan = Sedan()
    sedan.drive()
    sedan.honk()
}