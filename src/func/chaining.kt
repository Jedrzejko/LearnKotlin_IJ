package func

fun main(args: Array<String>) {
    val inputRows = listOf(
            mapOf("in01.csv" to listOf(1, 2, -3, 4, -5)),
            mapOf("in02.csv" to listOf(2, 4, -5, 6 - 1)),
            mapOf("in03.csv" to listOf(-1, -3, -4 - 3 - 4)),
            mapOf("in04.csv" to listOf(1, 2, 3, 4, 5)),
            mapOf("in04.csv" to listOf())
    )
    val cleaned = inputRows.flatMap { it.values }.flatten().filter { it in 0..3 }.toIntArray()
    println(cleaned.joinToString())
}