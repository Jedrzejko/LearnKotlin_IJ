package func

inline fun modifyString(str: String, operation: (String) -> String): String {
    return operation(str)
}

fun main(args: Array<String>) {
    modifyString("it is super", { it.toUpperCase() })
}