package oop

class Robot(val name: String) {
    fun greetHuma() {
        println("Hello Junk. Me be: $name")
    }

    fun knwosItsName() : Boolean = name.isNotBlank()
}

fun main(args: Array<String>) {
    val fghtRobot = Robot("Destro999")
    if (fghtRobot.knwosItsName()) fghtRobot.greetHuma()
}