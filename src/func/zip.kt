package func

fun main(argd: Array<String>) {
    val list = listOf("asd", "sdf", "dfg", "fgh")
    val containsD = listOf(true, true, true, false)
    val zipped: List<Pair<String, Boolean>> = list.zip(containsD)
    val mapping = list.zip(list.map { it.contains("d") })
    println(zipped)
    println(mapping)
}