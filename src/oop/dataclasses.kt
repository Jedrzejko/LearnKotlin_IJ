package oop

// Generates hashCode, equals, toString, copy, destructuring op
data class Address(val street: String, val nr: Int, val pstCode: String, val city: String)

fun main(args: Array<String>) {
    val residence = Address("long", 25, "12234", "Great")
    val residence2 = Address("long", 25, "12234", "Great")
    println(residence)
    println(residence2)
    //ref check
    println(residence === residence2)
    //struct check
    println(residence == residence2)

    val neighbor = residence.copy(nr = 23)
    println(neighbor)

    residence.component1()
    val (street, nr, pstCode, city) = residence
    println("$street, $nr, $pstCode, $city")
}