package func

import java.io.File

fun main(args: Array<String>) {
    //scoping
    File("example.txt").bufferedReader().let {
        if (it.ready()) {
            println(it.readLine())
        }
    }

    //wrk with nullables
    val str: String? = "Kot4And"
    str?.let {
        if (str.isNotEmpty()) {
            str.toLowerCase()
        }
    }
}