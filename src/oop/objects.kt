package oop

import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import oop.House.Companion.getNormalHouse as getHouse

object CountryFactory {
    val a = 4
    fun createCountry() = Country("Poland", 25)
}

object DefaultClickListener : MouseAdapter() {
    override fun mouseClicked(e: MouseEvent?) {
        println("mouseClicked")
    }
}

fun main(args: Array<String>) {
    println(CountryFactory.a)
    val country = CountryFactory.createCountry()
    println(country.name)
    println(getHouse().price)
}