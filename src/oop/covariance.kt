package oop

open class Being
open class Person : Being()
class Student : Person()

fun main(args: Array<String>) {
    // Covariance = ability to use more derived type (subclass)
    val people: MutableList<Person> = mutableListOf(Person(), Person())
    people.add(Student())

    val p: Any = Person() //cov

    //Read-only collections are covariant
    val students: List<Person> = listOf<Student>()

    //non covariant
    //val students2: MutableList<Person> = mutableListOf<Student>()
}
